const UrlSwitch = () => {
  const link = "server";
  // const link = "local";
  switch (link) {
    case "server":
      return "https://pokelogy-server.herokuapp.com";
    case "local":
      return "http://localhost:8000";
    default:
      return "http://localhost:8000";
  }
};

export default UrlSwitch;
