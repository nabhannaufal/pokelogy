import React, { useState, useEffect } from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Container,
  NavbarText,
} from "reactstrap";

const NavbarPoke = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const [sticky, setSticky] = useState(false);

  const toggle = () => setIsOpen(!isOpen);
  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
  }, []);

  const handleScroll = () => {
    if (window.scrollY > 90) {
      setSticky(true);
    } else if (window.scrollY < 90) {
      setSticky(false);
    }
  };

  return (
    <div className={`header${sticky ? " sticky" : ""}`}>
      <Navbar dark expand="md">
        <Container>
          <NavbarBrand href="/">
            <h4 className="text-oren">Pokelogy</h4>
          </NavbarBrand>
          <NavbarToggler onClick={toggle} />
          <Collapse isOpen={isOpen} navbar>
            <Nav navbar className="m-auto ">
              <NavItem>
                <NavLink href="/">
                  <h6 className="text-white">List Pokemon</h6>
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/mypokemon">
                  <h6 className="text-white">My Pokemon</h6>
                </NavLink>
              </NavItem>
            </Nav>
            <Nav navbar>
              <NavbarText>
                <h6 className="text-white">Welcome!</h6>
              </NavbarText>
            </Nav>
          </Collapse>
        </Container>
      </Navbar>
    </div>
  );
};

export default NavbarPoke;
