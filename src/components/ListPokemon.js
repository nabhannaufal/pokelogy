import { Container, Row, Input, Card, Col, CardTitle } from "reactstrap";
import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import axios from "axios";

function ListPokemon() {
  const [allData, setAllData] = useState([]);
  const [filterData, setFilterData] = useState(allData);
  const handleSearch = (event) => {
    let value = event.target.value.toLowerCase();
    let result = [];
    result = allData.filter((data) => {
      return data.name.search(value) !== -1;
    });
    setFilterData(result);
  };

  useEffect(() => {
    const fetchData = async () => {
      const pokemons = await axios.get("https://pokeapi.co/api/v2/pokemon");
      setAllData(pokemons.data.results);
      setFilterData(pokemons.data.results);
    };
    fetchData();
  }, []);
  return (
    <div className="bg-main">
      <Container>
        <Row className="mt-2 justify-content-center">
          <Col>
            <Input
              className="white-box search-bar"
              type="text"
              onChange={(event) => handleSearch(event)}
              placeholder="Search"
            />
          </Col>
        </Row>
        <Row className="mt-4">
          {filterData.map((item, index) => {
            if (item) {
              return (
                <Col md={4} key={index}>
                  <Link to={"/" + item.name} style={{ textDecoration: "none" }}>
                    <Card body className="text-center mb-4 btn-grad">
                      <CardTitle tag="h5" key={index}>
                        {item.name}
                      </CardTitle>
                    </Card>
                  </Link>
                </Col>
              );
            } else {
              return <h1>Data loading</h1>;
            }
          })}
        </Row>
      </Container>
    </div>
  );
}

export default ListPokemon;
