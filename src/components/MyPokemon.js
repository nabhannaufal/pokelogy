import {
  Container,
  Table,
  Row,
  Input,
  Col,
  Button,
  Modal,
  Form,
} from "reactstrap";
import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import UrlSwitch from "./UrlSwitch";
import axios from "axios";

function MyPokemon() {
  const link = UrlSwitch();
  const history = useHistory();
  const [id, setId] = useState(1);
  const [status, setStatus] = useState();
  const [alasan, setAlasan] = useState("");
  const [allData, setAllData] = useState([]);
  const [nickname, setNickname] = useState("");
  const [modalRename, setModalRename] = useState(false);
  const [modalDelete, setModalDelete] = useState(false);
  const [modalRelease, setModalRelease] = useState(false);
  const toggleRename = () => setModalRename(!modalRename);
  const toggleDelete = () => setModalDelete(!modalDelete);
  const toggleRelease = () => setModalRelease(!modalRelease);

  useEffect(() => {
    const fetchData = async () => {
      const pokemons = await axios.get(`${link}/api/mypokemon`);
      setAllData(pokemons.data.data);
    };
    const getPokemon = async () => {
      const getPokemons = await axios.get(`${link}/api/mypokemon/${id}`);
      setNickname(getPokemons.data.pokemon.nickname);
    };
    fetchData();
    getPokemon();
  }, [id, link]);

  const handleRename = async (e) => {
    e.preventDefault();
    await axios
      .put(`${link}/api/rename/${id}`, { nickname: nickname })
      .then(function (response) {
        console.log(response);
        history.push("/sukses");
      })
      .catch((error) => {
        console.error("There was an error!", error);
      });
  };

  const handleDelete = async (e) => {
    e.preventDefault();
    await axios
      .delete(`${link}/api/mypokemon/${id}`)
      .then(function (response) {
        console.log(response);
        history.push("/sukses");
      })
      .catch((error) => {
        console.error("There was an error!", error);
      });
  };

  const handleRelease = async (e) => {
    e.preventDefault();
    await axios
      .get(`${link}/api/release/${id}`)
      .then(function (response) {
        console.log(response);
        setStatus(response.status);
        setAlasan(response.data.alasan);
        toggleRelease();
      })
      .catch((error) => {
        console.error("There was an error!", error);
      });
  };
  const handleModal = (input) => {
    switch (input) {
      case 202:
        return (
          <Row className="modal-box">
            <div className="d-flex justify-content-between mb-2">
              <div className="title text-secondary align-self-center ">
                <h5>
                  Release <b>Succcess!</b>{" "}
                  <i className="far fa-laugh-squint"></i>
                </h5>
              </div>
              <i onClick={toggleRelease} className="fas fa-times icon-edit"></i>
            </div>
            <div className="d-flex justify-content-center mb-2">
              <div className="title text-success text-center">
                <h6>
                  <b>Congratulation!</b> {alasan} &#128527;
                </h6>
              </div>
            </div>
            <div className="d-flex column justify-content-between mt-3">
              <Button color="danger" href="/">
                Catch Again <i className="fas fa-hand-paper"></i>
              </Button>
              <Button color="success" href="/mypokemon">
                See Pokemon <i className="far fa-eye"></i>
              </Button>
            </div>
          </Row>
        );
      default:
        return (
          <Row className="modal-box">
            <div className="d-flex justify-content-between mb-2">
              <div className="title text-secondary align-self-center text-center">
                <h5>
                  Release <b>Failed!</b> <i className="far fa-frown-open"></i>
                </h5>
              </div>
              <i onClick={toggleRelease} className="fas fa-times icon-edit"></i>
            </div>
            <div className="d-flex justify-content-center mb-2">
              <div className="title text-danger text-center">
                <h6>
                  <b>Ouch!</b> {alasan} &#128557;
                </h6>
              </div>
            </div>
            <div className="d-flex column justify-content-between mt-3">
              <Button color="danger" href="/">
                Give Up <i className="fas fa-tired"></i>
              </Button>
              <Button color="success" onClick={toggleRelease}>
                Try Again <i className="fas fa-smile-wink"></i>
              </Button>
            </div>
          </Row>
        );
    }
  };

  return (
    <div className="bg-main">
      <Container>
        <Row className="justify-content-center">
          <Col md={6} className="white-box-2">
            <Table hover responsive>
              <thead>
                <tr className="text-center">
                  <th>#</th>
                  <th>Name</th>
                  <th>Success</th>
                  <th>Action</th>
                  <th>Release</th>
                </tr>
              </thead>
              <tbody>
                {allData.map((item, index) => {
                  if (item) {
                    if (!item.is_rename) {
                      return (
                        <tr key={index}>
                          <th className="text-center" scope="row">
                            {index + 1}
                          </th>
                          <td>{item.nickname}</td>
                          <td className="text-center">Never</td>
                          <td className="text-center">
                            <i
                              onClick={() => {
                                setId(item.id);
                                toggleRename();
                              }}
                              className="fas fa-edit text-success icon-edit"
                            >
                              {" "}
                            </i>
                            <i
                              onClick={() => {
                                setId(item.id);
                                toggleDelete();
                              }}
                              className="fas fa-trash-alt text-danger icon-edit"
                            >
                              {" "}
                            </i>
                          </td>
                          <td>
                            <btn
                              onClick={(e) => {
                                setId(item.id);
                                handleRelease(e);
                              }}
                              className="btn-evo"
                            >
                              Release
                            </btn>
                          </td>
                        </tr>
                      );
                    } else {
                      return (
                        <tr key={index}>
                          <th className="text-center" scope="row">
                            {index + 1}
                          </th>
                          <td>
                            {item.nickname}-{item.release}
                          </td>
                          <td className="text-center">
                            {item.initial_release}
                          </td>
                          <td className="text-center">
                            <i
                              onClick={() => {
                                setId(item.id);
                                toggleRename();
                              }}
                              className="fas fa-edit text-success icon-edit"
                            >
                              {" "}
                            </i>
                            <i
                              onClick={() => {
                                setId(item.id);
                                toggleDelete();
                              }}
                              className="fas fa-trash-alt text-danger icon-edit"
                            >
                              {" "}
                            </i>
                          </td>
                          <td>
                            <btn
                              onClick={(e) => {
                                setId(item.id);
                                handleRelease(e);
                              }}
                              className="btn-evo"
                            >
                              Release
                            </btn>
                          </td>
                        </tr>
                      );
                    }
                  } else {
                    return <h6>Data loading</h6>;
                  }
                })}
              </tbody>
            </Table>
          </Col>
        </Row>
        <div>
          <Modal isOpen={modalRename} toggle={toggleRename}>
            <Row className="modal-box">
              <div className="d-flex justify-content-between mb-2">
                <div className="title text-success align-self-center text-center">
                  <h5>
                    <b>Change Pokemon Name</b>
                  </h5>
                </div>
                <i
                  onClick={toggleRename}
                  className="fas fa-times icon-edit"
                ></i>
              </div>
              <Form onSubmit={(e) => handleRename(e)}>
                <Input
                  value={nickname}
                  onChange={(e) => setNickname(e.target.value)}
                ></Input>
                <div className="d-flex column justify-content-between mt-3">
                  <Button color="danger" onClick={toggleRename}>
                    Cancel <i className="fas fa-times-circle"></i>
                  </Button>
                  <Button color="success" type="submit">
                    Rename <i className="fas fa-pencil-alt"></i>
                  </Button>
                </div>
              </Form>
            </Row>
          </Modal>
          <Modal isOpen={modalDelete} toggle={toggleDelete}>
            <Row className="modal-box">
              <div className="d-flex justify-content-between mb-2">
                <div className="title text-danger align-self-center text-center">
                  <h5>
                    <b>Delete Pokemon </b>
                  </h5>
                </div>
                <i
                  onClick={toggleDelete}
                  className="fas fa-times icon-edit"
                ></i>
              </div>
              <div className="d-flex justify-content-center mb-2">
                <div className="title text-mute text-center">
                  <h6>
                    <b>Warning!</b> are you sure want to delete {nickname} ?{" "}
                    &#128552;
                  </h6>
                </div>
              </div>
              <div className="d-flex column justify-content-between mt-3">
                <Button color="success" onClick={toggleDelete}>
                  Cancel <i className="fas fa-undo-alt"></i>
                </Button>
                <Button color="danger" onClick={handleDelete}>
                  Delete <i className="fas fa-trash"></i>
                </Button>
              </div>
            </Row>
          </Modal>
          <Modal isOpen={modalRelease} toggle={toggleRelease}>
            {handleModal(status)}
          </Modal>
        </div>
      </Container>
    </div>
  );
}

export default MyPokemon;
