import React, { useState, useEffect } from "react";
import { useParams } from "react-router";
import UrlSwitch from "./UrlSwitch";
import axios from "axios";
import {
  Button,
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardBody,
  CardTitle,
  CardSubtitle,
  CardText,
  Modal,
} from "reactstrap";

const DetailPokemon = () => {
  const link = UrlSwitch();
  const { pokemonName } = useParams();
  const [move, setMove] = useState([]);
  const [type, setType] = useState([]);
  const [photo, setPhoto] = useState("");
  const [modalCatch, setModalCatch] = useState(false);
  const [status, setStatus] = useState();
  const [message, setMessage] = useState("");
  const toggleCatch = () => setModalCatch(!modalCatch);
  useEffect(() => {
    const fetchData = async () => {
      const getData = await axios.get(
        `https://pokeapi.co/api/v2/pokemon/${pokemonName}`
      );
      setMove(getData.data.moves);
      setPhoto(getData.data.sprites.other["official-artwork"].front_default);
      setType(getData.data.types);
    };

    fetchData();
  }, [pokemonName]);

  const handleCatch = async (e) => {
    e.preventDefault();
    await axios
      .post(`${link}/api/catch`, { pokemon_name: pokemonName })
      .then(function (response) {
        setMessage(response.data.message);
        setStatus(response.status);
        toggleCatch();
        console.log(response);
      })
      .catch((error) => {
        console.error("There was an error!", error);
      });
  };

  const handleModal = (input) => {
    switch (input) {
      case 201:
        return (
          <Row className="modal-box">
            <div className="d-flex justify-content-between mb-2">
              <div className="title text-secondary align-self-center ">
                <h5>
                  <b>Catch Result</b>
                </h5>
              </div>
              <i onClick={toggleCatch} className="fas fa-times icon-edit"></i>
            </div>
            <div className="d-flex justify-content-center mb-2">
              <div className="title text-success text-center">
                <h6>
                  <b>Congrats!</b> {message} &#128512;
                </h6>
              </div>
            </div>
            <div className="d-flex column justify-content-between mt-3">
              <Button color="danger" href="/">
                Try Again <i className="fas fa-sync-alt"></i>
              </Button>
              <Button color="success" href="/mypokemon">
                See Pokemon <i className="far fa-eye"></i>
              </Button>
            </div>
          </Row>
        );
      default:
        return (
          <Row className="modal-box">
            <div className="d-flex justify-content-between mb-2">
              <div className="title text-secondary align-self-center text-center">
                <h5>
                  <b>Hasil Tangkapan</b>
                </h5>
              </div>
              <i onClick={toggleCatch} className="fas fa-times icon-edit"></i>
            </div>
            <div className="d-flex justify-content-center mb-2">
              <div className="title text-danger text-center">
                <h6>
                  <b>Ouch!</b> {message} &#128557;
                </h6>
              </div>
            </div>
            <div className="d-flex column justify-content-between mt-3">
              <Button color="danger" href="/">
                Give Up<i className="fas fa-tired"></i>
              </Button>
              <Button color="success" onClick={toggleCatch}>
                Try Again <i className="fas fa-smile-wink"></i>
              </Button>
            </div>
          </Row>
        );
    }
  };

  return (
    <div className="bg-main">
      <Container>
        <Row className="justify-content-center">
          <Col md={4}>
            <Card className="white-box">
              <CardImg top width="100%" src={photo} alt="Card image cap" />
              <CardBody>
                <CardTitle tag="h5">{pokemonName}</CardTitle>
                <CardSubtitle tag="h6" className="mb-2 text-muted">
                  Type:{" "}
                  {type.map((item, index) => {
                    if (item) {
                      return (
                        <li key={index} style={{ display: "inline" }}>
                          {item.type.name},{" "}
                        </li>
                      );
                    } else {
                      return <h1>Data loading</h1>;
                    }
                  })}
                </CardSubtitle>
                <CardText>
                  Moves:{" "}
                  {move.slice(0, 9).map((item, index) => {
                    if (item) {
                      return (
                        <li key={index} style={{ display: "inline" }}>
                          {item.move.name},{" "}
                        </li>
                      );
                    } else {
                      return <h1>Data loading</h1>;
                    }
                  })}
                </CardText>
                <div className="d-flex column justify-content-between mt-3">
                  <Button color="danger" href="/">
                    Cancel <i className="fas fa-times-circle"></i>
                  </Button>
                  <Button color="success" onClick={(e) => handleCatch(e)}>
                    Catch <i className="fas fa-hand-paper"></i>
                  </Button>
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <div>
          <Modal isOpen={modalCatch} toggle={toggleCatch}>
            {handleModal(status)}
          </Modal>
        </div>
      </Container>
    </div>
  );
};

export default DetailPokemon;
