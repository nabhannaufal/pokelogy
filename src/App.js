import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import ListPokemonPages from "./components/pages/ListPokemonPages";
import MyPokemonPages from "./components/pages/MyPokemonPages";
import DetailPokemon from "./components/DetailPokemon";
import NavbarPoke from "./components/NavbarPoke";
import React from "react";
function App() {
  return (
    <Router>
      <NavbarPoke />
      <Switch>
        <Redirect exact from="/sukses" to="/mypokemon" />
        <Route path="/" exact component={ListPokemonPages} />
        <Route path="/mypokemon" exact component={MyPokemonPages} />
        <Route path="/:pokemonName" exact component={DetailPokemon} />
      </Switch>
    </Router>
  );
}

export default App;
